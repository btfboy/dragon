package com.dragon.user.controller;

import com.dragon.common.result.Result;
import com.dragon.user.service.IUUserService;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author Author
 * @since 2021-04-08
 */
@RestController
@RequestMapping("/user")
public class UUserController {

    @DubboReference(retries = 0, version = "${dubbo.consumer.IUUserService.version}")
    IUUserService userService;

    @GetMapping("/{unionId}")
    public Result user(@PathVariable("unionId") String unionId) {
        System.out.println(unionId);
        return userService.getUserById(unionId);
    }

}
