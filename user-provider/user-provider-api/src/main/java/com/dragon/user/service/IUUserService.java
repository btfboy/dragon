package com.dragon.user.service;

import com.dragon.common.result.Result;


/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Author
 * @since 2021-04-08
 */
public interface IUUserService {

    Result getUserById(String unionId);
}
