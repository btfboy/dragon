package com.dragon.user.dao.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author Author
 * @since 2021-04-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="UUser对象", description="")
public class UUser implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "唯一id")
    @TableId(value = "id", type = IdType.AUTO)
    private String id;

    private String username;

    private String phone;

    @ApiModelProperty(value = "0-女 1-男 2-未知")
    private Integer gender;

    private LocalDateTime birthday;

    @ApiModelProperty(value = "地址")
    private String address;

    @ApiModelProperty(value = "余额")
    private Long balance;

    @ApiModelProperty(value = "身份证")
    private String card;

    @ApiModelProperty(value = "等级")
    private Integer level;

    @ApiModelProperty(value = "经验值")
    private Integer experience;

    private LocalDateTime createDate;


}
