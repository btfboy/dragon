package com.dragon.user.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dragon.user.dao.entity.UUser;


/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Author
 * @since 2021-04-08
 */
public interface UUserMapper extends BaseMapper<UUser> {

}
