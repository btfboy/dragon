package com.dragon.user.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dragon.common.result.Result;
import com.dragon.user.dao.entity.UUser;
import com.dragon.user.dao.mapper.UUserMapper;
import com.dragon.user.service.IUUserService;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author Author
 * @since 2021-04-08
 */
@Service
@DubboService(version = "${dubbo.provider.IUUserService.version}")
public class UUserServiceImpl extends ServiceImpl<UUserMapper, UUser> implements IUUserService {
    @Autowired
    private UUserMapper userMapper;

    @Override
    public Result getUserById(String unionId) {
        return Result.success(userMapper.selectById(unionId));
    }
}
