package com.dragon.user;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@MapperScan("com.dragon.user.dao.mapper")
@SpringBootApplication
public class UserProviderController {
    public static void main(String[] args) {
        SpringApplication.run(UserProviderController.class, args);
    }
}
