package com.dragon.common.result;

import lombok.Data;

import java.io.Serializable;

@Data
public class Result<T> implements Serializable {
    private Integer code;
    private T data;
    private String msg;

    public static <T> Result<T> success(T t) {
        return new Result(t, 200, "成功!");
    }

    public static <T> Result fail() {
        return new Result(null, 139, "系统异常!");
    }

    public Result(T data, Integer code, String msg) {
        this.code = code;
        this.data = data;
        this.msg = msg;
    }
}
